﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K_means_Generater._2d
{
    class Data2d
    {
        public int Number { get; set; }
        public int XPoint { get; set; }
        public int YPoint { get; set; }

        public Data2d(int number, int xPoint, int yPoint, Cluster2d cluster)
        {
            Number = number;
            XPoint = xPoint;
            YPoint = yPoint;
            Cluster = cluster;
        }

        public Cluster2d Cluster { get; set; }

        public double Distace { get; set; }
    }
}
