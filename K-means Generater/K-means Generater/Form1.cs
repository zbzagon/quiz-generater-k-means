﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using K_means_Generater._2d;

namespace K_means_Generater
{
    public partial class Form1 : Form
    {
        List<Data2d> listData;
        List<Cluster2d> listCluster2d;
        DataTable dt;
        public Form1()
        {
            InitializeComponent();
        }

        private void bt_random_Click(object sender, EventArgs e)
        {
            int total = Int32.Parse(tb_amount.Text);
            Random rnd = new Random();
            dt = new DataTable();
            dt.Columns.Add("ID");
            dt.Columns.Add("X");
            dt.Columns.Add("Y");
            if (radioButton1.Checked)
            {
                for (int i = 0; i <= total - 1; i++)
                {
                    if (i == 0)
                    {
                        int x = rnd.Next(1, 11);
                        int y = rnd.Next(1, 11);
                        dt.Rows.Add(i + 1, x, y);
                        listData.Add(new Data2d(i + 1, x, y, null));
                    }
                    else
                    {
                        int x = rnd.Next(1, 11);
                        int y = rnd.Next(1, 11);
                        for (; ; )
                        {
                            if (Check_dup(x, y))
                            {
                                x = rnd.Next(1, 11);
                                y = rnd.Next(1, 11);
                            }
                            else
                            {
                                dt.Rows.Add(i + 1, x, y);
                                listData.Add(new Data2d(i + 1, x, y, null));
                                break;
                            }
                        }
                    }
                }
            }
            dataGridView1.DataSource = dt;
        }

        private bool Check_dup(int x, int y)
        {
            bool result = false;
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                if (Int32.Parse(dt.Rows[i][1].ToString()) == x && Int32.Parse(dt.Rows[i][2].ToString()) == y)
                {
                    //MessageBox.Show("Found Dup Data in "+(i+1)+ "  X : " + x + " Y : " + y );
                    result = true;
                    break;
                }
            }
            return result;
        }

        private void bt_findcluster_Click(object sender, EventArgs e)
        {

        }

        private void CreateClusters()
        {
            // Recreate list of cluster for reset
            listCluster2d = new List<Cluster2d>();
            Random rand = new Random();

            // Create clusters and add to list
            for (int i = 0; i < Int32.Parse(tb_cluster.Text); i++)
            {
                int xPoint = rand.Next(1, 11);
                int yPoint = rand.Next(1, 11);

                Cluster2d cluster = new Cluster2d(i + 1, xPoint, yPoint);
                listCluster2d.Add(cluster);
            }
        }
    }
}
